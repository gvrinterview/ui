# UI

Exercises concerning user interfaces.

## Exercise

The candidate shall design the UI from the following description:

*An interface to browse and select promotional coupons*

The UI shall respect these requirements:

- You can see and browse a list of coupons
- If the space is not enough to fit all coupons, the coupons shall be paginated
- You can select one or more coupons, the UI will show the sum of their values
- Each coupon have a title, a subtitle and a numeric value
- The UI shall be usable with only the touch screen
- The target persona is a retail cashier

The candidate can use their preferite tool and approach and form factor.
